def read_file(file_to_read):
    with open(file_to_read) as file:
        for line in file:
            array = [int(x) for x in line.split()]
        return array


def file_overload(result):
    if result // 10000000 > 0:      # мы устанавливаем порог: если при делении на 10 млн. результат > 0, то
        return str(result)          # храним результат как строковый тип данных
    else:
        return result


def find_min(d):
    min_value = d[0]
    for i in d:
        if i < min_value:
            min_value = i
    return file_overload(min_value)


def find_max(a):
    max_value = a[0]
    for i in a:
        if i > max_value:
            max_value = i
    return file_overload(max_value)


def find_sum(t):
    summa = 0
    for i in t:
        summa += i
    return file_overload(summa)


def find_product(a):
    product = 1
    for p in a:
        product *= p
    return file_overload(product)


if __name__ == '__main__':
    data = read_file("scratch.txt")
    print("Минимальное:", find_min(data))
    print("Максимальное:", find_max(data))
    print("Сумма:", find_sum(data))
    print("Произведение:", find_product(data))
