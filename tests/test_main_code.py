from main_code import find_sum, find_max, find_min, find_product, read_file
import numpy as np


def test_find_sum():
    data = read_file("scratch.txt")
    assert int(find_sum(data)) == sum(data)


def test_find_min():
    data = read_file("scratch.txt")
    assert int(find_min(data)) == min(data)


def test_find_max():
    data = read_file("scratch.txt")
    assert int(find_max(data)) == max(data)


def test_find_product():
    data = read_file("scratch.txt")
    assert int(find_product(data)) == np.prod(data)
