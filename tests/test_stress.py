from main_code import find_sum, find_max, find_min, find_product
from test_code_speed import get_random_array
import numpy as np
from random import seed, randint


def test_stress_sum():
    seed(25)

    for i in range(10_000):
        num1 = randint(-100, 100)
        num2 = randint(1000, 10000)
        num3 = randint(1, 5)
        rand_arr = get_random_array(num1, num2, num3)
        assert int(find_sum(rand_arr)) == sum(rand_arr)


def test_stress_min():
    seed(25)

    for i in range(10_000):
        num1 = randint(-100, 100)
        num2 = randint(1000, 10000)
        num3 = randint(1, 5)
        rand_arr = get_random_array(num1, num2, num3)
        assert int(find_min(rand_arr)) == min(rand_arr)


def test_stress_max():
    seed(25)

    for i in range(10_000):
        num1 = randint(-100, 100)
        num2 = randint(1000, 10000)
        num3 = randint(1, 5)
        rand_arr = get_random_array(num1, num2, num3)
        assert int(find_max(rand_arr)) == max(rand_arr)


def test_stress_product():
    seed(25)

    for i in range(10_000):
        num1 = randint(-100, 100)
        num2 = randint(1000, 10000)
        num3 = randint(1, 5)
        rand_arr = get_random_array(num1, num2, num3)
        assert int(find_product(rand_arr)) == np.prod(rand_arr)
