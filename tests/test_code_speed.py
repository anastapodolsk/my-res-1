from main_code import find_sum, find_max, find_min, find_product
import datetime
from random import randint, seed


def get_random_array(minimum, maximum, nums):
    seed(4)
    
    random_array = [randint(minimum, maximum) for _ in range(nums)]
    return random_array


def test_speed_min():
    seed(4)

    array1 = get_random_array(1, 101, 5)
    array2 = get_random_array(1, 10000, 10000)
    start_func1 = datetime.datetime.now()
    int(find_min(array1))
    start_func2 = datetime.datetime.now()
    int(find_min(array2))
    end_func2 = datetime.datetime.now()
    assert (end_func2 - start_func2) > (start_func2 - start_func1)


def test_speed_max():
    seed(4)

    array1 = get_random_array(1, 101, 5)
    array2 = get_random_array(1, 10000, 10000)
    start_func1 = datetime.datetime.now()
    int(find_max(array1))
    start_func2 = datetime.datetime.now()
    int(find_max(array2))
    end_func2 = datetime.datetime.now()
    assert (end_func2 - start_func2) > (start_func2 - start_func1)


def test_speed_product():
    seed(4)

    array1 = get_random_array(1, 101, 5)
    array2 = get_random_array(1, 10000, 10000)
    start_func1 = datetime.datetime.now()
    int(find_product(array1))
    start_func2 = datetime.datetime.now()
    int(find_product(array2))
    end_func2 = datetime.datetime.now()
    assert (end_func2 - start_func2) > (start_func2 - start_func1)


def test_speed_sum():
    seed(4)
    
    array1 = get_random_array(1, 101, 5)
    array2 = get_random_array(1, 10000, 10000)
    start_func1 = datetime.datetime.now()
    int(find_sum(array1))
    start_func2 = datetime.datetime.now()
    int(find_sum(array2))
    end_func2 = datetime.datetime.now()
    assert (end_func2 - start_func2) > (start_func2 - start_func1)
